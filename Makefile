include /usr/share/dpkg/pkg-info.mk

GITVERSION:=$(shell git rev-parse HEAD)

# amd64 only for now! else we need to generate d/control
DEB=proxmox-secure-boot-support_$(DEB_VERSION)_amd64.deb

BUILD_DIR=proxmox-secure-boot-support_$(DEB_VERSION)
DSC=proxmox-secure-boot-support_$(DEB_VERSION).dsc

.PHONY: deb dsc
deb: $(DEB)
dsc: $(DSC)

$(BUILD_DIR): debian
	rm -rf $@ $@.tmp
	mkdir $@.tmp
	cp -a debian $@.tmp/
	mv $@.tmp $@

$(DEB): $(BUILD_DIR)
	cd $(BUILD_DIR); dpkg-buildpackage -b -uc -us
	lintian $(DEB)

$(DSC): $(BUILD_DIR)
	cd $(BUILD_DIR); dpkg-buildpackage -S -uc -us
	lintian $(DSC)

sbuild: $(DSC)
	sbuild $(DSC)

.PHONY: upload
upload: UPLOAD_DIST ?= $(DEB_DISTRIBUTION)
upload: $(DEB)
	tar cf - $(DEB)|ssh repoman@repo.proxmox.com -- upload --product pve,pmg,pbs --dist $(UPLOAD_DIST)

.PHONY: clean distclean
distclean: clean
clean:
	rm -rf *~ proxmox-secure-boot-support*/ proxmox-secure-boot-support*.tar.* *.deb *.dsc *.changes *.buildinfo *.build
